## Multithreading and distributed programming in C++

### Additonal information

#### login and password for VM:

```
dev  /  tymczasowe
```

#### reinstall VBox addon

```
sudo /etc/init.d/vboxadd setup
```

#### GIT

```
git clone https://bitbucket.org/infotraining/cpp-dist-2016-05-17
```

[git cheat sheet](http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf)

#### Libs

```
git clone https://github.com/alanxz/rabbitmq-c
git clone https://github.com/alanxz/SimpleAmqpClient
```

Build rabbitmq-c first!

```
cd rabbitmq-c
mkdir build && cd build
cmake ..
cmake --build .
sudo cmake --build . --target install
```
then:

```
cd ../../SimpleAmqpClient
mkdir build && cd build
cmake ..
cmake --build .
sudo cmake --build . --target install
```


### Linki

http://blog.codinghorror.com/the-infinite-space-between-words/

http://bronikowski.com/2504/niesamowita-laska

http://www.1024cores.net/

http://preshing.com/

http://herbsutter.com/

http://www.cs.wustl.edu/~schmidt/ACE.html

https://software.intel.com/en-us/intel-tbb

http://stackoverflow.com/questions/11227809/why-is-processing-a-sorted-array-faster-than-an-unsorted-array

http://blog.bfitz.us/?p=491 - fibers, cooperative scheduling

http://lwn.net/Articles/250967/ - What every programmer should know about memory
