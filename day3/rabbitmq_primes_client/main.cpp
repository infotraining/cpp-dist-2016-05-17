#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <thread>

using namespace std;
using namespace AmqpClient;

bool is_prime(long n)
{
    for (long div = 2 ; div < n ; ++div)
        if( n % div == 0) return false;
    return true;
}

void go()
{
    Channel::ptr_t channel;
    channel = Channel::Create("test.czterybity.pl",
                              5672, "szkolenie", "tymczasowe");
    channel->BasicConsume("Q_SUSP", "", true, false, false, 1);
    for(;;)
    {
        auto env = channel->BasicConsumeMessage();
        cout << "got " << env->Message()->Body() << endl;
        long n = stol(env->Message()->Body());
        if (is_prime(n))
        {
            channel->BasicPublish("X_PRIMES", "",
                                  BasicMessage::Create(string("Leszek: ") + to_string(n)));
        }
        channel->BasicAck(env);
    }
}

int main(int argc, char *argv[])
{
    go();
    return 0;
}
