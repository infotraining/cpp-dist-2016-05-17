#include <iostream>
#include <zmq.hpp>
#include <zmqpp/zmqpp.hpp>
#include <thread>

using namespace std;

int main(int argc, char *argv[])
{
    zmqpp::context context;
    zmqpp::socket socket(context, zmqpp::socket_type::router);
    socket.bind("tcp://*:8888");

    std::cout << "starting" << std::endl;
    //for(;;)
    {
        for(int i = 0 ; i < 768 ; ++i)
        {
            zmqpp::message msg;
            socket.receive(msg);
            string addr, tmp, id;
            msg >> addr >> tmp >> id;
            //
            zmqpp::message rep;
            rep << addr << "";
            rep << to_string(i) + string(" 4");
            socket.send(rep);
            cout << "got req from " << addr << " " << id << " sent him line " << i << endl;
        }
    }
    std::cout << "finishing" << std::endl;
    return 0;
}
