#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <thread>

using namespace std;
using namespace AmqpClient;

void reader()
{
    Channel::ptr_t channel;
    channel = Channel::Create("test.czterybity.pl",
                              5672, "szkolenie", "tymczasowe");

    // -- this is The Way
    string qname = channel->DeclareQueue("");
    channel->BindQueue(qname, "chat");
    channel->BasicConsume(qname, "", true, true, false);
    for(;;)
    {
        BasicMessage::ptr_t msg = channel->BasicConsumeMessage()->Message();
        cout  << msg->Body() << endl;
    }
}

void send()
{
    Channel::ptr_t channel;
    channel = Channel::Create("test.czterybity.pl",
                              5672, "szkolenie", "tymczasowe");
    for(;;)
    {
        string msg;
        cout << ">>> ";
        getline(cin, msg);
        msg = "[Leszek] " + msg;
        channel->BasicPublish("chat", "", BasicMessage::Create(msg));
    }
}

int main(int argc, char *argv[])
{
    thread threader(reader);
    thread thsender(send);
    threader.join();
    thsender.join();
    return 0;
}
