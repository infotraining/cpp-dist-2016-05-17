#include <iostream>
#include <zmq.hpp>
#include <zmqpp/zmqpp.hpp>
#include <thread>

using namespace std;

int main(int argc, char *argv[])
{
    zmqpp::context context;
    zmqpp::socket socket(context, zmqpp::socket_type::pub);
    socket.bind("tcp://*:9191");
    this_thread::sleep_for(1s);
    cout << "kill sent" << endl;
    socket.send("KILL");
    this_thread::sleep_for(1s);
    return 0;
}
