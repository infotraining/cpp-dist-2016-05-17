#include <iostream>
#include <zmq.hpp>
#include <zmqpp/zmqpp.hpp>
#include <thread>

using namespace std;

void process(zmqpp::context& context)
{
    zmqpp::socket socket(context, zmqpp::socket_type::rep);
    socket.connect("inproc://workers");

    for(;;)
    {
        zmqpp::message msg;
        cout << "waiting for message" << endl;
        socket.receive(msg);
        std::string text;
        msg >> text;
        cout << "got request " << text << endl;
        this_thread::sleep_for(5s);
        socket.send("Hello from server");
        cout << "message sent" << endl;
    }

}

int main(int argc, char *argv[])
{
    zmqpp::context context;
    zmqpp::socket in_socket(context, zmqpp::socket_type::router);
    in_socket.bind("tcp://*:6666");

    zmqpp::socket proc_socket(context, zmqpp::socket_type::dealer);
    proc_socket.bind("inproc://workers");

    vector<thread> workers;
    for(int i = 0 ; i < 4 ; ++i)
    {
        workers.emplace_back(process, ref(context));
    }

    //zmq::proxy(static_cast<void*>(in_socket),
    //           static_cast<void*>(proc_socket), NULL);
    zmqpp::poller poller;
    poller.add(in_socket);
    poller.add(proc_socket);
    for(;;)
    {
        if(poller.poll())
        {
            if (poller.has_input(in_socket))
            {
                zmqpp::message msg;
                in_socket.receive(msg);
                proc_socket.send(msg);
            }
            if (poller.has_input(proc_socket))
            {
                zmqpp::message msg;
                proc_socket.receive(msg);
                in_socket.send(msg);
            }
        }
    }

    for(auto& th : workers) th.join();

    cout << "Hello World!" << endl;
    return 0;
}
