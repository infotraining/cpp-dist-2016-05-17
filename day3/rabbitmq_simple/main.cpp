#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>

using namespace std;
using namespace AmqpClient;

void setup()
{
    Channel::ptr_t channel;
    channel = Channel::Create("test.czterybity.pl",
                              5672, "szkolenie", "tymczasowe");
    channel->DeclareExchange("input", Channel::EXCHANGE_TYPE_DIRECT);
    channel->DeclareQueue("q", false, false, false, false);
    channel->BindQueue("q", "input");

}

void send_message()
{
    Channel::ptr_t channel;
    channel = Channel::Create("test.czterybity.pl",
                              5672, "szkolenie", "tymczasowe");
    channel->BasicPublish("input", "", BasicMessage::Create("Hello from Leszek"));
}

void get_message()
{
    Channel::ptr_t channel;
    channel = Channel::Create("test.czterybity.pl",
                              5672, "szkolenie", "tymczasowe");
    channel->BasicConsume("q", "", true, true, false);
    for(;;)
    {
        BasicMessage::ptr_t msg = channel->BasicConsumeMessage()->Message();
        cout << "got " << msg->Body() << endl;
    }
}

int main(int argc, char *argv[])
{
    //setup();
    //send_message();
    get_message();
    return 0;
}

/*
cd ~/libs/rabbitmq-c
mkdir build
cd build/
cmake ..
cmake --build .
sudo cmake --build . --target install

cd ~/libs/SimpleAmqpClient
mkdir build
cd build/
cmake ..
cmake --build .
sudo cmake --build . --target install

sudo nano /etc/ld.so.conf.d/x86_64-linux-gnu.conf
add line:
/usr/local/lib/x86_64-linux-gnu

sudo ldconfig
*/
