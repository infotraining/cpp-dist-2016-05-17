#include <iostream>
#include <zmq.hpp>
#include <zmqpp/zmqpp.hpp>
#include <thread>
#include <string>
#include <sstream>
#include "smallpt_lib.hpp"

using namespace std;

int main(int argc, char *argv[])
{
    zmqpp::context context;
    zmqpp::socket in_socket(context, zmqpp::socket_type::req);
    zmqpp::socket out_socket(context, zmqpp::socket_type::push);
    zmqpp::socket kill_socket(context, zmqpp::socket_type::sub);
    in_socket.set(zmqpp::socket_option::identity, "leszek_socket");
    in_socket.connect("tcp://test.czterybity.pl:8888");
    out_socket.connect("tcp://test.czterybity.pl:8889");
    kill_socket.connect("tcp://test.czterybity.pl:9191");
    kill_socket.set(zmqpp::socket_option::subscribe, "");

    zmqpp::poller poll;
    poll.add(in_socket);
    poll.add(kill_socket);

    in_socket.send("Leszek is asking for line");

    for(;;)
    {
        if (poll.poll()) // we have events on sockets
        {
            if(poll.has_input(in_socket))
            {
                string task;
                in_socket.receive(task);
                istringstream iss(task);
                int line_no, quality;
                iss >> line_no >> quality;
                string line = scan_line(line_no, quality);

                // "Leszek 123 :: 1 2 3 45 66 77 88 99 ..."

                string out = string("Leszek: ") + to_string(line_no)
                        + " :: " + line;
                cout << "processed line " << line_no << endl;
                out_socket.send(out);
                in_socket.send("Leszek is asking for line");
            }
            if(poll.has_input(kill_socket))
            {
                return -1;
            }
        }
    }
    return 0;
}
