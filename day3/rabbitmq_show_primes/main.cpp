#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <thread>

using namespace std;
using namespace AmqpClient;

void go()
{
    Channel::ptr_t channel;
    channel = Channel::Create("test.czterybity.pl",
                              5672, "szkolenie", "tymczasowe");
    channel->BasicConsume("Q_PRIMES");
    for(;;)
    {
        auto msg = channel->BasicConsumeMessage()->Message();
        cout << msg->Body() << endl;
    }
}

int main(int argc, char *argv[])
{
    go();
    return 0;
}
