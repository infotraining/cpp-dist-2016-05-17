#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <thread>

using namespace std;
using namespace AmqpClient;

void setup()
{
    Channel::ptr_t channel;
    channel = Channel::Create("test.czterybity.pl",
                              5672, "szkolenie", "tymczasowe");
    channel->DeclareExchange("X_PRIMES", Channel::EXCHANGE_TYPE_DIRECT);
    channel->DeclareExchange("X_SUSP", Channel::EXCHANGE_TYPE_DIRECT);
    channel->DeclareQueue("Q_SUSP", false, false, false, false);
    channel->DeclareQueue("Q_PRIMES", false, false, false, false);
    channel->BindQueue("Q_PRIMES", "X_PRIMES" );
    channel->BindQueue("Q_SUSP", "X_SUSP" );

    for(int i = 100000; i < 1000000 ; ++i)
    {
        channel->BasicPublish("X_SUSP", "",
                              BasicMessage::Create(to_string(i)));
    }
}

int main(int argc, char *argv[])
{
    setup();
    return 0;
}
