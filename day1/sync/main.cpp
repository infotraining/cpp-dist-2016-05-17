#include <iostream>
#include <vector>
#include <thread>
#include <algorithm>
#include <atomic>

using namespace std;

class Data
{
    vector<int> data_;
    //bool is_ready{false};
    atomic<bool> is_ready{false};
public:
    void prepare()
    {
        data_.resize(20);
        cout << "reading data" << endl;
        generate(data_.begin(), data_.end(), [] { return rand() %100; });
        this_thread::sleep_for(20s);
        is_ready = true;
    }

    void process()
    {
        for(;;)
        {
            if(is_ready)
            {
                cout << "processing" << endl;
                cout << "sum = " << accumulate(data_.begin(), data_.end(), 0);
                cout << endl;
                return;
            }
            this_thread::yield();
        }
    }
};


int main()
{
    Data d;
    thread thp(&Data::prepare, &d);
    thread thc1(&Data::process, &d);
    thread thc2(&Data::process, &d);
    cout << "Working on GUI" << endl;
    thp.join();
    thc1.join();
    thc2.join();
    return 0;
}

