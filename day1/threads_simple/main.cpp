#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void hello()
{
    cout << "Hello world" << endl;
}

struct F
{
    void operator()()
    {
        cout << "Hello world from operator ()" << endl;
    }
};

void param_fun(int a)
{
    cout << "function with " << a << endl;
}

void param_return(int& a)
{
    cout << "modyfing function with " << a << endl;
    a = 100;
}

thread generate_thread()
{
    thread th(&hello);
    return th;
}

int main(int argc, char *argv[])
{
    cout << "Hello threads!" << endl;
    cout << "hw concurrency " << thread::hardware_concurrency() << endl;
    thread th1(&hello);
    F f;
    thread th2(f);
    thread th3(param_fun, 10);
    int z;
    thread th4(param_return, std::ref(z));
    th3.join();
    th1.join();
    th2.join();
    th4.join();
    cout << z << endl;
    //move semantic
    thread th5 = generate_thread();
    vector<thread> th;
    th.push_back(move(th5));
    th.push_back(generate_thread());
    th.emplace_back(generate_thread());
    th.emplace_back(param_fun, 12);
    for(auto &t : th) t.join();
    return 0;
}




