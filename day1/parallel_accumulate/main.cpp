#include <iostream>
#include <thread>
#include <vector>
#include <algorithm>
#include <chrono>

using namespace std;

template<typename It, typename T>
void ref_accumulate(It b, It e, T init, T& res)
{
    res = accumulate(b, e, init);
}

template<typename It, typename T>
T parallel_accumulate(It b, It e, T init)
{
    unsigned int n_of_threads = thread::hardware_concurrency();
    unsigned int size = std::distance(b, e);
    vector<T> res(n_of_threads); // result vector
    vector<thread> threads;
    unsigned int bl_size = size/n_of_threads;

    It bl_start = b;
    for(unsigned int i = 0 ; i < n_of_threads ; ++i)
    {
        It bl_end = bl_start;
        advance(bl_end, bl_size);
        if( i == n_of_threads -1) bl_end = e;
        threads.emplace_back(ref_accumulate<It,T>, bl_start, bl_end, T(), ref(res[i]));
        bl_start = bl_end;
    }

    for(auto& th : threads) th.join();

    return accumulate(res.begin(), res.end(), init);
}

int main(int argc, char *argv[])
{
    const int N = 1000000;
    vector<long> v;
    for(int i = 0 ; i < N; ++i)
    {
        v.push_back(i);
    }
    cout << "sum = " << accumulate(v.begin(), v.end(), 0L) << endl;
    cout << "sum = " << parallel_accumulate(v.begin(), v.end(), 0L) << endl;

    return 0;
}
