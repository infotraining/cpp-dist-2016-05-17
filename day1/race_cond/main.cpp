#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <atomic>
#include <chrono>

using namespace std;

long counter{0};
long counter_mtx{0};
atomic<long> counter_at{0};
mutex mtx_q;

void increment()
{
    for(int i = 0 ; i < 1000000 ; ++i)
        ++counter;
}

void increment_mtx()
{
    for(int i = 0 ; i < 1000000 ; ++i)
    {
        mtx_q.lock();
        ++counter_mtx;
        mtx_q.unlock();
    }
}

void increment_atomic()
{
    for(int i = 0 ; i < 1000000 ; ++i)
    {
        ++counter_at;
    }
}


int main(int argc, char *argv[])
{
    auto start = chrono::high_resolution_clock::now();
    vector<thread> threads;
    for (int i = 0 ; i < 4 ; ++i)
        threads.emplace_back(increment);
    for (auto& th : threads) th.join();

    auto end = chrono::high_resolution_clock::now();

    cout << "res = " << counter << endl;
    cout << "time = " << chrono::duration_cast<chrono::microseconds>(end-start).count() << endl;

    threads.clear();

    start = chrono::high_resolution_clock::now();

    for (int i = 0 ; i < 4 ; ++i)
        threads.emplace_back(increment_mtx);
    for (auto& th : threads) th.join();

    end = chrono::high_resolution_clock::now();


    cout << "res mutex = " << counter_mtx << endl;
    cout << "time = " << chrono::duration_cast<chrono::microseconds>(end-start).count() << endl;



    threads.clear();

    start = chrono::high_resolution_clock::now();

    for (int i = 0 ; i < 4 ; ++i)
        threads.emplace_back(increment_atomic);
    for (auto& th : threads) th.join();

    end = chrono::high_resolution_clock::now();


    cout << "res atomic = " << counter_at << endl;
    cout << "is really lock free? " << counter_at.is_lock_free() << endl;
    cout << "time = " << chrono::duration_cast<chrono::microseconds>(end-start).count() << endl;





    return 0;
}

