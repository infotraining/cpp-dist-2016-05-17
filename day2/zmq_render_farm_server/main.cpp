#include <iostream>
#include <zmq.hpp>
#include <zmqpp/zmqpp.hpp>
#include <thread>

using namespace std;

int main(int argc, char *argv[])
{
    zmqpp::context context;
    zmqpp::socket socket(context, zmqpp::socket_type::push);
    socket.bind("tcp://*:8888");

    for(;;)
    {
        for(int i = 0 ; i < 768 ; ++i)
        {
            this_thread::sleep_for(200ms);
            socket.send(to_string(i) + string(" 4"));
        }
    }

    return 0;
}
