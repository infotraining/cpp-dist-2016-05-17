#include <iostream>
#include <zmq.hpp>
#include <zmqpp/zmqpp.hpp>
#include <thread>
#include <string>
#include <sstream>

using namespace std;

int main(int argc, char *argv[])
{
    zmqpp::context context;
    zmqpp::socket in_socket(context, zmqpp::socket_type::pull);
    zmqpp::socket out_socket(context, zmqpp::socket_type::push);
    in_socket.connect("tcp://test.czterybity.pl:8888");
    out_socket.connect("tcp://test.czterybity.pl:8889");
    for(;;)
    {
        string task;
        in_socket.receive(task);
        istringstream iss(task);
        int line_no, quality;
        iss >> line_no >> quality;
        string line = scan_line(line_no, quality);

        // "Leszek 123 :: 1 2 3 45 66 77 88 99 ..."

        string out = string("Leszek: ") + to_string(line_no)
                + " :: " + line;
        out_socket.send(out);
    }
    return 0;
}
