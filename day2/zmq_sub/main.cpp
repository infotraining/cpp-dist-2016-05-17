#include <iostream>
#include <zmq.hpp>
#include <zmqpp/zmqpp.hpp>
#include <thread>

using namespace std;

int main(int argc, char *argv[])
{
    zmqpp::context context;
    zmqpp::socket socket(context, zmqpp::socket_type::sub);

    socket.connect("tcp://test.czterybity.pl:7777");
    socket.set(zmqpp::socket_option::subscribe, "");

    for(;;)
    {
        string msg;
        socket.receive(msg);
        cout << msg << endl;
    }
    return 0;
}
