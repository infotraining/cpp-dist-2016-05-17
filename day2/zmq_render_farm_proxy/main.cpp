#include <iostream>
#include <zmq.hpp>
#include <zmqpp/zmqpp.hpp>
#include <thread>

using namespace std;

int main(int argc, char *argv[])
{
    zmqpp::context context;
    zmqpp::socket in_socket(context, zmqpp::socket_type::pull);
    zmqpp::socket out_socket(context, zmqpp::socket_type::push);

    in_socket.bind("tcp://*:8889");
    out_socket.bind("tcp://*:9999");
    for(;;)
    {
        string msg;
        in_socket.receive(msg);
        cout << "got " << msg << endl;
        out_socket.send(msg);
    }
    return 0;
}
