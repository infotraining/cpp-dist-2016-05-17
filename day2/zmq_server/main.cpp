#include <iostream>
#include <zmq.hpp>
//#include <zhelpers.hpp>
#include <zmqpp/zmqpp.hpp>
#include <thread>

using namespace std;

int main(int argc, char *argv[])
{
    cout << "Hello World" << endl;
    zmqpp::context context;
    zmqpp::socket socket(context, zmqpp::socket_type::rep);

    socket.bind("tcp://*:6666");

    for(;;)
    {
        zmqpp::message msg;
        cout << "waiting for message" << endl;
        socket.receive(msg);        
        std::string text;
        msg >> text;
        cout << "got request " << text << endl;
        this_thread::sleep_for(5s);
        socket.send("Hello from server");
        cout << "message sent" << endl;
    }
    return 0;
}
