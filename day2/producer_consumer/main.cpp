#include <iostream>
#include <vector>
#include <thread>
#include <string>
#include <queue>
#include <mutex>
#include <condition_variable>

using namespace std;

queue<int> q;
mutex mtx_q;
condition_variable cond_q;

void producer()
{
    for (int i = 0 ; i < 100 ; ++i)
    {
        {
            lock_guard<mutex> lg(mtx_q);
            q.push(i);
            cond_q.notify_one();
        }
        this_thread::sleep_for(100ms);
    }
}

void consumer(int id)
{
    for(;;)
    {
        unique_lock<mutex> lg(mtx_q);
        while(q.empty())
        {
            cond_q.wait(lg);
        }
        int i = q.front();
        q.pop();
        cout << id << " got " << i << endl;
    }
}

int main(int argc, char *argv[])
{
    cout << "Hello World!" << endl;
    vector<thread> threads;
    threads.emplace_back(producer);
    threads.emplace_back(consumer, 1);
    threads.emplace_back(consumer, 2);
    for(auto& th : threads) th.join();
    return 0;
}
