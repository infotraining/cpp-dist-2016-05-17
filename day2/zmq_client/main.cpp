#include <iostream>
#include <zmq.hpp>
#include <zmqpp/zmqpp.hpp>
#include <thread>

using namespace std;

int main(int argc, char *argv[])
{
    cout << "ZMQ client" << endl;
    zmqpp::context context;
    zmqpp::socket socket(context, zmqpp::socket_type::req);

    socket.connect("tcp://localhost:6666");

    for(int i = 0 ; i < 5 ; ++i)
    {
        zmqpp::message msg;
        std::string text;
        cout << "sending message" << endl;
        socket.send("Hello dear server, this is Leszek");
        cout << "message is send" << endl;
        socket.receive(msg);
        msg >> text;
        cout << "got answer " << text << endl;

    }
    return 0;
}
