#include <iostream>
#include <zmq.hpp>
#include <zmqpp/zmqpp.hpp>
#include <thread>

using namespace std;

int main(int argc, char *argv[])
{
    zmqpp::context context;
    zmqpp::socket socket(context, zmqpp::socket_type::pub);

    socket.bind("tcp://*:7777");

    for (long i = 0 ; i < 100000000 ; ++i)
    {
        string data("current number ");
        data = data + to_string(i);
        socket.send(data);
        this_thread::sleep_for(500ms);
    }

    return 0;
}
