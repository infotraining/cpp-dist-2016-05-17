#include <mutex>
#include <condition_variable>
#include <queue>

template<class T>
class thread_safe_queue
{
    std::queue<T> q_;
    std::mutex mtx_;
    std::condition_variable cond_;

public:
    thread_safe_queue() { }

    void push(T item)
    {
        std::lock_guard<std::mutex> lg(mtx_);
        q_.push(item);
        cond_.notify_one();
    }

    T pop()
    {
        std::unique_lock<std::mutex> lg(mtx_);
        while(q_.empty())
        {
            cond_.wait(lg);
        }
        T item = q_.front();
        q_.pop();
        return item;
    }
};
