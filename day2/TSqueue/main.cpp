#include <iostream>
#include <thread>
#include "tsq.h"

using namespace std;

thread_safe_queue<Task*> q;

void producer()
{
    for (int i = 0 ; i < 100 ; ++i)
    {
        q.push(i);
        this_thread::sleep_for(100ms);
    }
}

void consumer(int id)
{
    for(;;)
    {
        int i = q.pop();
        cout << id << " got " << i << endl;
    }
}

int main(int argc, char *argv[])
{
    cout << "Hello World!" << endl;
    vector<thread> threads;
    threads.emplace_back(producer);
    threads.emplace_back(consumer, 1);
    threads.emplace_back(consumer, 2);
    for(auto& th : threads) th.join();
    return 0;
}
