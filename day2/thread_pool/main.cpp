#include <iostream>
#include "tsq.h"
#include <functional>
#include <vector>
#include <thread>

using namespace std;

class thread_pool
{
    thread_safe_queue<std::function<void()>> q;
    vector<thread> workers;

    void worker()
    {
        for(;;)
        {
            std::function<void()> task = q.pop();
            if (!task) return;
            task();
        }
    }

public:
    thread_pool()
    {
        for (int i = 0 ; i < 4 ; ++i)
        {
            workers.emplace_back(&thread_pool::worker, this);
        }

    }

    void submit(std::function<void()> task)
    {
        q.push(task);
    }

    ~thread_pool()
    {
        for (int i = 0 ; i < 4 ; ++i)
            q.push(std::function<void()>());
        for(auto& th : workers) th.join();
    }
};

int main(int argc, char *argv[])
{
    thread_pool tp;
    for (int i = 0 ; i < 10 ; ++i)
        tp.submit([i]() {
             cout << "hello world " << i << endl;
             this_thread::sleep_for(1s);}
        );

    return 0;
}
